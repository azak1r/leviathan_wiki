<?php

Route::group([ 
    'namespace'     => 'BinaryTorch\LaRecipe\Http\Controllers',
    'middleware'    => ['auth', 'can:global.superuser'],
    'prefix'        => 'wiki'
], function () {

    // Logic here
});