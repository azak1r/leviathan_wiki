<?php

return [
    'create' => [
        'label'       => 'Create wiki docs',
        'description' => 'Create wiki docs'
    ],
    'read' => [
        'label'       => 'Read wiki docs',
        'description' => 'Read wiki docs'
    ],
    'update' => [
        'label'       => 'Edit wiki docs',
        'description' => 'Edit wiki docs'
    ],
    'delete' => [
        'label'       => 'Delete wiki docs',
        'description' => 'Delete wiki docs'
    ],
];