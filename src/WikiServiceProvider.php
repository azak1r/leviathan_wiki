<?php

namespace Azak1r\Wiki;

use Seat\Services\AbstractSeatPlugin;

class WikiServiceProvider extends AbstractSeatPlugin
{
     /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->add_routes();
    }

    public function register()
    {
        $this->registerPermissions(__DIR__ . '/Config/Permissions/wiki.permissions.php', 'wiki');
    }

     /**
     * Include the routes.
     */
    public function add_routes()
    {

        if (! $this->app->routesAreCached()) {
            include __DIR__ . '/Http/routes.php';
        }
    }

    /**
     * Return the plugin public name as it should be displayed into settings.
     *
     * @return string
     */
    public function getName(): string
    {
        return 'Leviathan wiki';
    }

    /**
     * Return the plugin repository address.
     *
     * @return string
     */
    public function getPackageRepositoryUrl(): string
    {
        return '#';
    }

    /**
     * Return the plugin technical name as published on package manager.
     *
     * @return string
     */
    public function getPackagistPackageName(): string
    {
        return 'levi_wiki';
    }

    /**
     * Return the plugin vendor tag as published on package manager.
     *
     * @return string
     */
    public function getPackagistVendorName(): string
    {
        return 'azak1r';
    }
}